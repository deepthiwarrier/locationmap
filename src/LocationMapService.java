/**
 * LocationMapService.java
 * @Author: Deepthi Warrier
 * @StudentID: 1970749
 * @Date: 30/Nov/2019
 * The REST Service which calls a third party REST - Bing Maps service to get the map
 * of the location coordinates passed. *
 */

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

// Java class that will host the URI path "/"
@Path("/")
public class LocationMapService {

    // Java method will process the HTTP GET request
    @GET
    // This method will return a simple message of type HTML
    @Produces("text/html")
    public String displayMessage(){
        return "Welcome to my first Java REST Example";
    }

    // This method will connect to Bing Map Service
    // Calls the Bing Map service to retreive the map jpeg for the nearby hospitals.
    @Path("/map")
    @GET
    @Produces("image/jpeg")
    public Response getMap(){
        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/47.2446,-122.4376/11?mapSize=500,500&pp=47.2456,-122.4481;21;St.Josephs&pp=47.2594,-122.4530;21;MultiCareTacoma&pp=47.225259,-122.504258;21;BioMatUSA&pp=47.333051,-122.316974;21;BloodworksNorthwest&pp=47.183009,-122.501851;21;St.ClareHospital&key=Ak4wGkB-qpDqTCC9Ve6hBiadin0vlSlHvqrCu4U25Hm46fzRiQXdAn-qAqoQB2xC");
        Response response = myResource.request().get();
        return response;
    }
}
