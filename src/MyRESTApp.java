/**
 * MyRESTApp.java
 * @Author: Deepthi Warrier
 * @StudentID: 1970749
 * @Date: 30/Nov/2019
 * The REST Service which calls a third party RSET service to get the map
 * of the location coordinates passed. *
 */
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

//Defines the base URI for all resource URIs.
@ApplicationPath("/")
//The java class declares root resource and provider classes
public class MyRESTApp extends Application{
    //The method returns a non-empty collection with classes, that must be included in the published JAX-RS application
    @Override
    public Set<Class<?>> getClasses() {
        HashSet h = new HashSet<Class<?>>();
        h.add(LocationMapService.class);
        return h;
    }
}